from django.contrib import admin
from .models import Matkul, Partisipan, Kegiatan

# Register your models here.
admin.site.register(Matkul)
admin.site.register(Partisipan)
admin.site.register(Kegiatan)