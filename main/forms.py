from django import forms
from .models import Matkul

class Isian(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = [
            'namaMatkul',
            'dosenMatkul',
            'sksMatkul',
            'deskripsiMatkul',
            'semesterMatkul',
            'ruangMatkul'
        ]

        nama_attrs = {
            'type' : 'text',
            'placeholder' : 'Nama Mata Kuliah'
        }

        dosen_attrs = {
            'type' : 'text',
            'placeholder' : 'Nama Dosen Pengajar'
        }

        sks_attrs = {
            'type' : 'text',
            'placeholder' : 'Jumlah SKS'
        }

        deskripsi_attrs = {
            'type' : 'text',
            'placeholder' : 'Deskripsi'
        }

        semester_attrs = {
            'type' : 'text',
            'placeholder' : 'Semester'
        }

        ruang_attrs = {
            'type' : 'text',
            'placeholder' : 'Ruang'
        }

        namaMatkul = forms.CharField(label='', max_length=50, required=True, widget=forms.TextInput(attrs=nama_attrs))
        dosenMatkul = forms.CharField(label='', max_length=50, required=True, widget=forms.TextInput(attrs=dosen_attrs))
        sksMatkul = forms.CharField(max_length=3, label='', required=True, widget=forms.TextInput(attrs=sks_attrs))
        deskripsiMatkul = forms.CharField(label='', max_length=300, required=True, widget=forms.TextInput(attrs=deskripsi_attrs))
        semesterMatkul = forms.CharField(label='', max_length=50, required=True, widget=forms.TextInput(attrs=semester_attrs))
        ruangMatkul = forms.CharField(label='', max_length=50, required=True, widget=forms.TextInput(attrs=ruang_attrs))
