# Generated by Django 3.1.2 on 2020-10-24 14:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20201018_0004'),
    ]

    operations = [
        migrations.CreateModel(
            name='Kegiatan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('acara', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Partisipan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('objek', models.CharField(max_length=50)),
                ('kegiatan', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.kegiatan')),
            ],
        ),
    ]
