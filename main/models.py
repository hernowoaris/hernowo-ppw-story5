from django.db import models

# Create your models here.
class Matkul(models.Model):
    namaMatkul = models.CharField(max_length=50, null=True)
    dosenMatkul = models.CharField(max_length=50, null=True)
    sksMatkul = models.CharField(max_length=3, null=True)
    deskripsiMatkul = models.CharField(max_length=300, null=True)
    semesterMatkul = models.CharField(max_length=20, null=True)
    ruangMatkul = models.CharField(max_length=50, null=True)

class Kegiatan(models.Model):
    acara = models.CharField(max_length=50)

    def __str__(self):
        return self.acara

class Partisipan(models.Model):
    objek = models.CharField(max_length=50)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)

    def __str__(self):
        return self.objek

