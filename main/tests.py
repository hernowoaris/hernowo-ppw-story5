from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from .models import Partisipan, Kegiatan

# class TestURL(TestCase):
#     def test_kegiatan(self):
#         response = Client().get('/kegiatan')
#         self.assertEquals(response.status_code, 200)

#     def test_form_kegiatan(self):
#         response = Client().get('/isian-kegiatan')
#         self.assertEquals(response.status_code, 200)

#     def test_form_kegiatan(self):
#         response = Client().get('')
#         self.assertEquals(response.status_code, 200)


# class TestModels(TestCase):
#     def setUp(self):
#         self.acara = Kegiatan.objects.create(name="test")
    
#     def test_kegiatan(self):
#         self.assertEqual(Kegiatan.objects.all().count(), 1)


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:profile'))
        self.assertEqual(response.status_code, 200)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())