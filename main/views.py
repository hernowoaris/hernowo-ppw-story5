from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Matkul, Kegiatan, Partisipan
from .forms import Isian

def profile(request):
    return render(request, 'main/profile.html')

def sekolah(request):
    return render(request, 'main/sekolah.html')

def games(request):
    return render(request, 'main/games.html')

def story1(request):
    return render(request, 'main/story1.html')

def watchlist(request):
    return render(request, 'main/watchlist.html')

def matakuliah(request):
    matakuliah = Matkul.objects.all()
    return render(request, 'main/matakuliah.html', {"matakuliah" : matakuliah})

def isianMatkul(request):
    form = Isian()

    if request.method == 'POST':
        form = Isian(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/matakuliah')

    return render(request, 'main/forms.html', {'form':form})

def kegiatan(request):
    kegiatan_1 = Partisipan.objects.filter(kegiatan__name="Kegiatan 1")
    kegiatan_2 = Partisipan.objects.filter(kegiatan__name="Kegiatan 2")
    kegiatan_3 = Partisipan.objects.filter(kegiatan__name="Kegiatan 3")
    context = {'activity_1': activity_1, 'activity_2': activity_2, 'activity_3': activity_3,}
    return render(request, 'story4/activity.html', context)


def register(request):
    return render(request, 'main/isian-kegiatan.html')

    

