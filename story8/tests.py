from django.test import TestCase, Client
from .views import fungsi_halaman, fungsi_search
from django.urls import resolve

# Create your tests here.
class TestStory8(TestCase):
    def test_url(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code,200)

    def test_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, fungsi_halaman)


    def test_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8/story8.html')

    def test_dalam_template(self):
        response = Client().get('/story8/')
        html = response.content.decode("utf8")
        self.assertIn("Pencarian", html)
        self.assertIn("KEMBALI KE HALAMAN UTAMA", html)
        # self.assertJSONEqual(str(response.content, encoding='utf8'), html)

