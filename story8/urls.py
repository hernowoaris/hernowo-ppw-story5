from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.fungsi_halaman, name="halaman"),
    path('data/', views.fungsi_search, name="search"),
]