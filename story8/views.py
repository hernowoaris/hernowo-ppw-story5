from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

# Create your views here.

def fungsi_halaman(request):
    return render(request, 'story8/story8.html')

def fungsi_search(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    result = requests.get(url)
    print(result)
    data = json.loads(result.content)
    return JsonResponse(data, safe=False)