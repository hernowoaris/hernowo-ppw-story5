from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import AnonymousUser, User
from .views import registerPage, loginPage, logoutPage
from .forms import CreateUserForm

# Create your tests here.

class TestStory9(TestCase):
    def test_create_account(self):
        user = User.objects.create_user(username='hernowo12345', password='dummypassword')
        user.save()
        self.assertEqual(User.objects.count(), 1)

    def test_template_register(self):
        response = Client().get('/story9/register/')
        self.assertTemplateUsed(response, 'story9/register.html')

    def test_func_register(self):
        found = resolve('/story9/register/')
        self.assertEqual(found.func, registerPage)

    def test_func_login(self):
        found = resolve('/story9/login/')
        self.assertEqual(found.func, loginPage)
        
    def test_template_login(self):
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response, 'story9/login.html')

    def test_login_user(self):
        user = User.objects.create_user(username='hernowo12345', password='dummypassword')
        user.save()
        self.client = Client()
        login = self.client.login(username='hernowo12345', password='dummypassword')
        self.assertTrue(login)
    
    def test_login_redirect(self):
        user = User.objects.create_user(username='hernowo12345', password='dummypassword')
        user.save()
        c = Client()
        c.login(username='hernowo12345', password='dummypassword')
        response = c.get('/story9/login/')
        self.assertEqual(response.status_code, 302)




