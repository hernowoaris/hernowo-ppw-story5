from django.urls import path
from .views import registerPage, loginPage, logoutPage


app_name = 'story9'

urlpatterns = [
    path('login/', loginPage, name="login"),
    path('register/', registerPage, name="register"),
    path('logout/', logoutPage, name='logout')
]